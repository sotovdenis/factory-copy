package com.example.newfactory.dataAccessor;

import com.example.newfactory.entity.Detail;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataAccessorDetail {
    private Connection connection;
    private static DataAccessorDetail dataAccsessor = new DataAccessorDetail("jdbc:postgresql://localhost:5432/postgres", "postgres", "Ugegam16");

    public static DataAccessorDetail getDataAccsessor() {
        return dataAccsessor;
    }

    private DataAccessorDetail(String dbUrl, String userID, String password) {
        try {
            connection =  DriverManager.getConnection(dbUrl,userID,password);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public List<Detail> getDetailsList () throws SQLException{
        List<Detail> details = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM details;");

            while (resultSet.next()){
                int art = resultSet.getInt("art");
                String name = resultSet.getString("name");
                int price = resultSet.getInt("price");
                int count = resultSet.getInt("count");

                Detail detail = new Detail(art,name,price, count);
                details.add(detail);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return details;
    }

    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }

}
