package com.example.newfactory.dataAccessor;

import com.example.newfactory.entity.Accept;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataAccessorAccept {
    private Connection connection;
    private static DataAccessorAccept dataAccsessor = new DataAccessorAccept("jdbc:postgresql://localhost:5432/postgres", "postgres", "Ugegam16");

    public static DataAccessorAccept getDataAccsessor() {
        return dataAccsessor;
    }

    private DataAccessorAccept(String dbUrl, String userID, String password) {
        try {
            connection =  DriverManager.getConnection(dbUrl,userID,password);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public List<Accept> getAcceptList () throws SQLException{
        List<Accept> accepts = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accept;");

            while (resultSet.next()){
                int workerID = resultSet.getInt("workerid");
                int productID = resultSet.getInt("productid");

                Accept accept = new Accept(workerID,productID);
                accepts.add(accept);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return accepts;
    }

    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }
}
