package com.example.newfactory.dataAccessor;

import com.example.newfactory.entity.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataAccessorProducts {
    private Connection connection;
    private static DataAccessorProducts dataAccessor = new DataAccessorProducts("jdbc:postgresql://localhost:5432/postgres", "postgres", "Ugegam16");

    public static DataAccessorProducts getDataAccessor() {
        return dataAccessor;
    }

    private DataAccessorProducts(String dbUrl, String userID, String password) {
        if (dataAccessor == null) {
            try {
                connection = DriverManager.getConnection(dbUrl, userID, password);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    public List<Product> getProductsList() throws SQLException {
        List<Product> products = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM products;");


            while (resultSet.next()) {
                int art = resultSet.getInt("art");
                String name = resultSet.getString("name");
                int price = resultSet.getInt("price");
                int maxStage = resultSet.getInt("maxstage");
                int count = resultSet.getInt("count");

                Product product = new Product(art, name, price, maxStage, count);
                products.add(product);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return products;
    }


    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }
}
