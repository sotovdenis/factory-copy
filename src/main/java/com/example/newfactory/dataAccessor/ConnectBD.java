package com.example.newfactory.dataAccessor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectBD {

    public static void main(String[] args) {

        Connection connection = null;

        try {

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "Ugegam16");

            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            if (connection != null) {
                System.out.println("подключено");
            } else {
                System.out.println("аборт");
            }

            ResultSet table = statement.executeQuery("SELECT * FROM public.details;");

            table.beforeFirst();
            while (table.next()) {
                for (int i=1; i <= table.getMetaData().getColumnCount();i++){
                    System.out.print(table.getString(i)+" ");
                }
            }

            System.out.println();
            System.out.println();

            ResultSet tableLogin = statement.executeQuery("SELECT * FROM public.login;");

            tableLogin.beforeFirst();
            while (tableLogin.next()){
                for (int i = 1; i <= tableLogin.getMetaData().getColumnCount();i++){
                    System.out.print(tableLogin.getString(i) + " ");
                }
            }

            if (table != null){
                table.close();
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}


