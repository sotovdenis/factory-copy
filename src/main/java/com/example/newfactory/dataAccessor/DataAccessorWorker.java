package com.example.newfactory.dataAccessor;

import com.example.newfactory.entity.users.Worker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataAccessorWorker {


    private Connection connection;
    private static DataAccessorWorker dataAccessor = new DataAccessorWorker("jdbc:postgresql://localhost:5432/postgres", "postgres", "Ugegam16");

    public static DataAccessorWorker getDataAccessor() {
        return dataAccessor;
    }

    private DataAccessorWorker(String dbUrl, String userID, String password) {
        if (dataAccessor == null) {
            try {
                connection = DriverManager.getConnection(dbUrl, userID, password);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    public List<Worker> getWorkersList() throws SQLException {
        List<Worker> workers = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM login;");


            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String password = resultSet.getString("password");
                String name = resultSet.getString("name");
                String status = resultSet.getString("status");

                Worker worker = new Worker(id, password, name, status);
                workers.add(worker);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return workers;
    }


    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }
}
