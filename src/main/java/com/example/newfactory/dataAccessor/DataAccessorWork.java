package com.example.newfactory.dataAccessor;

import com.example.newfactory.entity.Work;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataAccessorWork {

    private Connection connection;
    private static DataAccessorWork dataAccsessor = new DataAccessorWork("jdbc:postgresql://localhost:5432/postgres", "postgres", "Ugegam16");

    public static DataAccessorWork getDataAccsessor() {
        return dataAccsessor;
    }

    private DataAccessorWork(String dbUrl, String userID, String password) {
        try {
            connection = DriverManager.getConnection(dbUrl, userID, password);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Work> getWorkList() throws SQLException {
        List<Work> work = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM work;");

            while (resultSet.next()) {
                int factoryID = resultSet.getInt("factoryId");
                int productID = resultSet.getInt("productId");
                String status = resultSet.getString("status");
                int stage = resultSet.getInt("stage");

                Work workingProcess = new Work(factoryID, productID, status, stage);
                work.add(workingProcess);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return work;
    }

    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }

}
