package com.example.newfactory.entity;

public class Work {
    public int factoryId;
    public int productId;
    public int stage;
    public String status;

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public int getProductId() {
        return productId;
    }

    public String isStatus() {
        return status;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Work(int factoryId, int productId, String status, int stage) {
        this.factoryId = factoryId;
        this.productId = productId;
        this.status = status;
        this.stage = stage;
    }
}
