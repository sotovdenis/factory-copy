package com.example.newfactory.entity;

public enum StatusOfWork {
    DONE,
    InProcess,
    NotStarted
}
