package com.example.newfactory.entity;

public class Security {
    private static final String BASE_ALPHABET = "АБВГДЕËЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ,. абвгдеёжзийклмнопрстуфхцчшщъыьэюяQWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm1234567890";
    private static final String ALPHABET1 = "QWERTYUIOPLKJHGFDSAZXCVBNMабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕËЖЗИЙКЛМН1234567890ОПРСТУФХЦЧШЩЪЫЬЭЮЯ,. qwertyuioplkjhgfdsazxcvbnm";
    private static final String ALPHABET2 = "АБВГДЕËЖабвгдеёжqwertyuioplkjhgfdsazxcvbnmЗИЙКЛМНОПРСТУФХЦQWE1234567890RTYUIOPLKJHGFDSAZXCVBNMЧШЩЪЫЬЭЮЯ,. зийклмнопрстуфхцчшщъыьэюя";
    private static final String ALPHABET3 = "СТУФХЦЧШЩЪЫЬЭЮЯ,. абвгдеёжзийклмнопрАБВQ1234567890WERTYUIOPqwertyuioplkjhgfdsazxcvbnmLKJHGFDSAZXCVBNMГДЕËЖЗИЙКЛМНОПРстуфхцчшщъыьэюя";

    public static String encrypt(String message) {
        StringBuilder cipherText = new StringBuilder();
        int count = 0;
        for (char c : message.toCharArray()) {
            if (BASE_ALPHABET.contains(String.valueOf(c))) {
                if (count % 3 == 0) {
                    int index = BASE_ALPHABET.indexOf(c);
                    cipherText.append(ALPHABET1.charAt(index));
                } else if (count % 3 == 1) {
                    int index = BASE_ALPHABET.indexOf(c);
                    cipherText.append(ALPHABET2.charAt(index));
                } else if (count % 3 == 2) {
                    int index = BASE_ALPHABET.indexOf(c);
                    cipherText.append(ALPHABET3.charAt(index));
                }
                count++;
            }
        }
        return cipherText.toString();
    }

    public static String decrypt(String message) {
        StringBuilder nonCipherText = new StringBuilder();
        int count = 0;
        for (char c : message.toCharArray()) {
            if (count % 3 == 0) {
                if (ALPHABET1.contains(String.valueOf(c))) {
                    int index = (ALPHABET1.indexOf(c));
                    nonCipherText.append(BASE_ALPHABET.charAt(index));
                    count++;
                }
            } else if (count % 3 == 1) {
                if (ALPHABET2.contains(String.valueOf(c))) {
                    int index = (ALPHABET2.indexOf(c));
                    nonCipherText.append(BASE_ALPHABET.charAt(index));
                    count++;
                }
            } else if (count % 3 == 2) {
                if (ALPHABET3.contains(String.valueOf(c))) {
                    int index = (ALPHABET3.indexOf(c));
                    nonCipherText.append(BASE_ALPHABET.charAt(index));
                    count++;
                }
            }
        }
        return nonCipherText.toString();
    }
}

