package com.example.newfactory.entity;

public class Detail {
    public String name;
    public int art;
    public int price;
    public int count;

    public Detail(int art, String name, int price, int count){
        this.art = art;
        this.name = name;
        this.price = price;
        this.count = count;

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArt(int art) {
        this.art = art;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getArt() {
        return art;
    }

    public int getPrice() {
        return price;
    }
}
