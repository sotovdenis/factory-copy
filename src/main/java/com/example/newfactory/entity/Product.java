package com.example.newfactory.entity;

public class Product {
    public String name;
    public int art;
    public int price;
    public int maxStage;
    public int count;

    public Product(int art, String name, int price, int maxStage, int count){
        this.art = art;
        this.name = name;
        this.price = price;
        this.maxStage = maxStage;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMaxStage() {
        return maxStage;
    }

    public void setMaxStage(int maxStage) {
        this.maxStage = maxStage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArt(int art) {
        this.art = art;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getArt() {
        return art;
    }

    public int getPrice() {
        return price;
    }


}
