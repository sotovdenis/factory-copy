package com.example.newfactory.entity.users;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Worker extends User {
    private int id;
    private String password;
    private String name;
    private String status;

    public List<Worker> test = new ArrayList<>();

    public static Worker getTest(List<Worker> test) throws SQLException {
        return new Worker(1,"12345678","Сотов Денис Русланович","admin");
    }

    public void setName(String name) {
        this.name = name;
    }

    public Worker(int id, String password, String name, String status) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.status = status;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }
}
