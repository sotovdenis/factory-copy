package com.example.newfactory.entity;

public class Accept {
    public int workerID;
    public int prID;

    public int getWorkerID() {
        return workerID;
    }

    public void setWorkerID(int workerID) {
        this.workerID = workerID;
    }

    public int getPrID() {
        return prID;
    }

    public void setPrID(int prID) {
        this.prID = prID;
    }

    public Accept(int workerID, int prID) {
        this.workerID = workerID;
        this.prID = prID;
    }
}
