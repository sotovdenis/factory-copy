package com.example.newfactory.controllers;

import com.example.newfactory.dataAccessor.DataAccessorDetail;
import com.example.newfactory.entity.Detail;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class HelloController {
    public TableColumn articul;
    public TableColumn name;
    public TableColumn price;
    public PasswordField adminPassword;
    public Label errorLabel;
    public Button loginAsworker;
    public PasswordField workerPassword;
    public Button loginAsEngineer;
    public PasswordField engineerPassword;
    @FXML
    private TableView<Detail> tvOrderTable;
    @FXML
    private Label welcomeText;
    @FXML
    private Label openReg;
    @FXML
    private Button signIn;
    @FXML
    private Button loginAsAdmin;
    @FXML
    private Button placeAnOrder;
    @FXML
    private Button createListDetails;

    @FXML
    private void createListDetails() {
        tvOrderTable.getColumns().clear();
        tvOrderTable.getItems().clear();
        DataAccessorDetail dataAccessorDetail = DataAccessorDetail.getDataAccsessor();

        TableColumn<Detail, String> artColumn = articul;
        TableColumn<Detail, String> nameColumn = name;
        TableColumn<Detail, String> priceColumn = price;


        artColumn.setCellValueFactory(new PropertyValueFactory<>("art"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        tvOrderTable.getColumns().addAll(artColumn, nameColumn, priceColumn);
        try {
            tvOrderTable.getItems().addAll(dataAccessorDetail.getDetailsList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    @FXML
    protected void onLoginAsEngineer() {
        if (engineerPasswordCheck()) {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/EngineerView.fxml"));

            try {
                fxmlLoader.load();
                //EngineerController.refresh();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Parent root = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Engineer");
            stage.showAndWait();
        }
    }

    @FXML
    private boolean adminPasswordCheck() {

        boolean result = false;

        if (adminPassword.getText().isBlank()) {
            adminPassword.clear();
            errorLabel.setText("Да мы так, наблюдаем");
        } else if (adminPassword.getText().equals("admin228")) {
            result = true;
        }
        return result;
    }

    @FXML
    private boolean workerPasswordCheck() {

        boolean result = false;

        if (workerPassword.getText().isBlank()) {
            workerPassword.clear();
            errorLabel.setText("Да мы так, наблюдаем");
        } else if (workerPassword.getText().equals("worker228")) {
            result = true;
        }
        return result;
    }

    @FXML
    private boolean engineerPasswordCheck() {

        boolean result = false;

        if (engineerPassword.getText().isBlank()) {
            engineerPassword.clear();
            errorLabel.setText("Да мы так, наблюдаем");
        } else if (engineerPassword.getText().equals("eng228")) {
            result = true;
        }
        return result;
    }

    @FXML
    void onBtnLoginAsAdmin() {
        if (adminPasswordCheck()) {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/AdminView.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Parent root = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Order");
            stage.showAndWait();
        }
    }

    @FXML
    public void onBtnLoginAsWorker(ActionEvent event) {

        if (workerPasswordCheck()) {

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/WorkerView.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Parent root = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Эй, служивый, мы стрелять не хотим, мы так - наблюдаем");
            stage.showAndWait();
        }
    }
}