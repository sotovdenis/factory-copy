package com.example.newfactory.controllers;

import com.example.newfactory.dataAccessor.DataAccessorAccept;
import com.example.newfactory.dataAccessor.DataAccessorProducts;
import com.example.newfactory.dataAccessor.DataAccessorWork;
import com.example.newfactory.entity.Accept;
import com.example.newfactory.entity.Product;
import com.example.newfactory.entity.Work;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EngineerController {

    public TableView productTable;
    public TableView workingProcessTable;
    public TextField addProductID;
    public TextField addProductName;
    public TextField addProductPrice;
    public Button onBtnAddProduct;
    public TableView productCtrl;
    public CheckBox accept;
    public Button uploadAccept;
    public Button refreshAll;
    public TableColumn productId;
    public TableColumn productName;
    public TableColumn productPrice;
    public TableColumn factoryID;
    public TableColumn productID;
    public TableColumn status;
    public TableColumn stage;
    public TableColumn workerID;
    public TableColumn productIdCtrl;
    public TextField setProductName;
    public TextField setProductPrice;
    public TextField workerIDCtrl;
    public TextField productIdCtrlText;
    public TextField addMaxStage;
    public TextField setMaxStagesUpload;
    public TableColumn productCount;
    public Button close;

    @FXML
    void refresh() {
        createListProducts();
        createWorkList();
        createListProductCtrl();
    }

    @FXML
    public void initialize() {
        refresh();
    }
    public void close(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }


    void createListProducts() {
        productTable.getColumns().clear();
        productTable.getItems().clear();
        DataAccessorProducts dataAccessorProducts = DataAccessorProducts.getDataAccessor();

        TableColumn<Product, String> name = productName;
        TableColumn<Product, String> art = productId;
        TableColumn<Product, String> price = productPrice;
        TableColumn<Product, String> count = productCount;


        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        art.setCellValueFactory(new PropertyValueFactory<>("art"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        count.setCellValueFactory(new PropertyValueFactory<>("count"));

        productTable.getColumns().addAll(art, name, price, count);
        try {
            productTable.getItems().addAll(dataAccessorProducts.getProductsList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    void createListProductCtrl() {
        productCtrl.getColumns().clear();
        productCtrl.getItems().clear();
        DataAccessorAccept dataAccessorAccept = DataAccessorAccept.getDataAccsessor();

        TableColumn<Accept, String> productIdToCtrl = productIdCtrl;
        TableColumn<Accept, String> workerId = workerID;

        workerId.setCellValueFactory(new PropertyValueFactory<>("workerID"));
        productIdToCtrl.setCellValueFactory(new PropertyValueFactory<>("prID"));

        productCtrl.getColumns().addAll(workerId, productIdToCtrl);
        try {
            productCtrl.getItems().addAll(dataAccessorAccept.getAcceptList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    void createWorkList() {
        workingProcessTable.getColumns().clear();
        workingProcessTable.getItems().clear();
        DataAccessorWork dataAccessorWork = DataAccessorWork.getDataAccsessor();

        TableColumn<Work, String> fID = factoryID;
        TableColumn<Work, String> productId1 = productID;
        TableColumn<Work, String> statusOfWork = status;
        TableColumn<Work, String> stages = stage;


        fID.setCellValueFactory(new PropertyValueFactory<>("factoryId"));
        productId1.setCellValueFactory(new PropertyValueFactory<>("productId"));
        statusOfWork.setCellValueFactory(new PropertyValueFactory<>("status"));
        stages.setCellValueFactory(new PropertyValueFactory<>("stage"));

        workingProcessTable.getColumns().addAll(fID, productId1, statusOfWork, stages);
        try {
            workingProcessTable.getItems().addAll(dataAccessorWork.getWorkList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void addProduct() {
        DataAccessorWork db = DataAccessorWork.getDataAccsessor();

        String request = "insert into products values (" + addProductID.getText() + ",'" + addProductName.getText() + "'," + addProductPrice.getText() + ", " + addMaxStage.getText() + ")";
        //TODO проверка на повтор айдишника
        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(request);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }

        addProductPrice.clear();
        addProductName.clear();
        addProductID.clear();
        addMaxStage.clear();
        refresh();
    }

    public void upload(ActionEvent event) {
        DataAccessorAccept dbAccept = DataAccessorAccept.getDataAccsessor();
        DataAccessorProducts dbProducts = DataAccessorProducts.getDataAccessor();
        DataAccessorWork dbWork = DataAccessorWork.getDataAccsessor();

        String requestToInsertIntoProducts = "insert into products values (" + productIdCtrlText.getText() + ", '" + setProductName.getText() + "', " + setProductPrice.getText() + ", " + setMaxStagesUpload.getText() + ")";
        String requestToDeleteFromAccept = "delete from accept where workerid = " + workerIDCtrl.getText() + " and productid = " + productIdCtrlText.getText() + "";
        String requestToDeleteFromWork = "delete from work where factoryid = " + workerIDCtrl.getText() + "";
        String requestToCheckAccept = "select count(1) from accept where workerid = " + workerIDCtrl.getText() + " and productid = " + productIdCtrlText.getText() + "";
        String requestToInsertIntoWorkBecauseWorkerIsStupidBitch = "insert into work values (" + workerIDCtrl.getText() + ",0, 'Not Started' ," + productIdCtrlText.getText() + ")";
        String requestToUpdateProducts = "update products set count = count + 1 where art = " + productIdCtrlText.getText() + "";
        String requestToCheckProducts = "select count(1) from products where art = " + productIdCtrlText.getText() + "";
        //String requestToUpdateWorkProgress = "update products set stage = " + StatusOfWork.DONE + " where art = " + productIdCtrlText.getText() + "";


        boolean checkAccept = false;
        boolean checkProducts = false;


        try {
            Statement statement = dbAccept.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToCheckAccept);
            while (resultSet.next()) {
                if (resultSet.getInt(1) >= 1) {
                    checkAccept = true;
                } else {
                    checkAccept = false;
                }
            }
        } catch (SQLException e) {
            e.getMessage();
        }

        if (accept.isSelected() && checkAccept) {

            try {
                Statement statement = dbAccept.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToDeleteFromAccept);
                System.out.println(resultSet);
            } catch (SQLException e) {
                e.getMessage();
            }
            try {
                Statement statement = dbWork.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToDeleteFromWork);
            } catch (SQLException e) {
                e.getMessage();
            }

            try {
                try {
                    Statement statement = dbAccept.createStatement();
                    ResultSet resultSet = statement.executeQuery(requestToCheckProducts);
                    while (resultSet.next()) {
                        if (resultSet.getInt(1) >= 1) {
                            checkProducts = true;
                        } else {
                            checkProducts = false;
                        }
                    }
                } catch (SQLException e) {
                    e.getMessage();
                }

                if (checkProducts) {
                    Statement statement = dbProducts.createStatement();
                    ResultSet resultSet = statement.executeQuery(requestToUpdateProducts);
                } else {
                    Statement statement = dbProducts.createStatement();
                    ResultSet resultSet = statement.executeQuery(requestToInsertIntoProducts);
                }
            } catch (SQLException e) {
                e.getMessage();
            }
            try {
                Statement statement = dbAccept.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToDeleteFromAccept);

            } catch (SQLException e) {
                e.getMessage();
            }
        } else {
            try {
                Statement statement = dbAccept.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToDeleteFromAccept);

            } catch (SQLException e) {
                e.getMessage();
            }
            try {
                Statement statement = dbWork.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToInsertIntoWorkBecauseWorkerIsStupidBitch);

            } catch (SQLException e) {
                e.getMessage();
            }
        }


        workerIDCtrl.clear();
        setProductName.clear();
        setProductPrice.clear();
        productIdCtrlText.clear();
        setMaxStagesUpload.clear();
        refresh();
    }
}
