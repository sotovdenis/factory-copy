package com.example.newfactory.controllers;

import com.example.newfactory.dataAccessor.DataAccessorDetail;
import com.example.newfactory.dataAccessor.DataAccessorProducts;
import com.example.newfactory.dataAccessor.DataAccessorWorker;
import com.example.newfactory.entity.Detail;
import com.example.newfactory.entity.Product;
import com.example.newfactory.entity.Security;
import com.example.newfactory.entity.users.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AdminController {

    public Button addWorker;
    public Button deleteFromWorkers;
    public TextField deleteFromWorkersText;
    public TableView loginTable;

    public TableColumn id;
    public TableColumn password;

    public TableColumn name;
    public TextField addIdWorkerText;
    public TextField addPasswordWorkerText;
    public TextField addNameWorkerText;
    public TableView detailTable;
    public TableColumn art;
    public TableColumn nameofdetail;
    public TableColumn priceOfDetail;
    public Button deleteFromDetails;
    public Button addDetail;
    public TableColumn artProduct;
    public TableColumn nameofproduct;
    public TableColumn priceOfProduct;
    public TableView productTable;
    public Button addProduct;
    public Button deleteFromProduct;
    public TextField maxStage;
    public TableColumn countOfProduct;
    public TableColumn status;
    public TextField addStatus;

    @FXML
    private Button back;

    @FXML
    private Font x1;

    @FXML
    private Color x2;

    @FXML
    private Font x3;

    @FXML
    private Color x4;

    public void close(ActionEvent event) {
        Stage stage = (Stage) back.getScene().getWindow();
        stage.close();
    }
    @FXML
    void refresh(){
        createListProducts();
        createListDetails();
        createListWorkers();
    }
    @FXML
     public void initialize(){
        refresh();
    }

    @FXML
    public void clear(){
        addStatus.clear();
        addIdWorkerText.clear();
        addPasswordWorkerText.clear();
        deleteFromWorkersText.clear();
        addNameWorkerText.clear();
        maxStage.clear();
    }
    @FXML
    void createListWorkers() { //вынести отсюда криэйты в функцию рефреш, которая стартует при закгрузке фиксэмэл
        loginTable.getColumns().clear();
        loginTable.getItems().clear();
        DataAccessorWorker dataAccessorWorker = DataAccessorWorker.getDataAccessor();

        TableColumn<Worker, String> fID = id;
        TableColumn<Worker, String> pword = password;
        TableColumn<Worker, String> name1 = name;
        TableColumn<Worker, String> statusOfLogin = status;


        fID.setCellValueFactory(new PropertyValueFactory<>("id"));
        pword.setCellValueFactory(new PropertyValueFactory<>("password"));
        name1.setCellValueFactory(new PropertyValueFactory<>("name"));
        statusOfLogin.setCellValueFactory(new PropertyValueFactory<>("status"));

        loginTable.getColumns().addAll(fID, pword, name1, statusOfLogin);
        try {
            loginTable.getItems().addAll(dataAccessorWorker.getWorkersList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    void createListDetails() {
        detailTable.getColumns().clear();
        detailTable.getItems().clear();
        DataAccessorDetail dataAccessorDetail = DataAccessorDetail.getDataAccsessor();

        TableColumn<Detail, String> articul = art;
        TableColumn<Detail, String> name = nameofdetail;
        TableColumn<Detail, String> price = priceOfDetail;


        articul.setCellValueFactory(new PropertyValueFactory<>("art"));
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));

        detailTable.getColumns().addAll(articul, name, price);
        try {
            detailTable.getItems().addAll(dataAccessorDetail.getDetailsList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    void createListProducts() {
        productTable.getColumns().clear();
        productTable.getItems().clear();
        DataAccessorProducts dataAccessorProducts = DataAccessorProducts.getDataAccessor();

        TableColumn<Product, String> name = artProduct;
        TableColumn<Product, String> art = nameofproduct;
        TableColumn<Product, String> price = priceOfProduct;
        TableColumn<Product, String> count = countOfProduct;


        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        art.setCellValueFactory(new PropertyValueFactory<>("art"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        count.setCellValueFactory(new PropertyValueFactory<>("count"));

        productTable.getColumns().addAll(art, name, price, count);
        try {
            productTable.getItems().addAll(dataAccessorProducts.getProductsList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    void addWorker() {
        DataAccessorWorker db = DataAccessorWorker.getDataAccessor();
        String encodedPassword = Security.encrypt(addPasswordWorkerText.getText());

        String request = "insert into login values (" + addIdWorkerText.getText() + ",'" + encodedPassword + "','" + addNameWorkerText.getText() + "', '" + addStatus.getText() + "')";
        //TODO проверка на повтор айдишника
        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(request);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }
        clear();
        refresh();
    }

    @FXML
    void deleteFromWorkers() {
        DataAccessorWorker db = DataAccessorWorker.getDataAccessor();

        String request = "delete from login where id = '" + deleteFromWorkersText.getText() + "';";

        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(request);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }
        clear();
        refresh();
    }
    @FXML
    void deleteFromDetails(ActionEvent event) {
        DataAccessorDetail db = DataAccessorDetail.getDataAccsessor();

        String requestToDeleteFromDetails = "delete from details where art = " + deleteFromWorkersText.getText() + ";";

        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToDeleteFromDetails);
        } catch (SQLException e) {
            e.getMessage();
        }
        clear();
        refresh();
    }
    @FXML
    void addProduct(ActionEvent event) {
        DataAccessorProducts db = DataAccessorProducts.getDataAccessor();
        String encodedPassword = Security.encrypt(password.getText());

        String request = "insert into products values (" + addIdWorkerText.getText() + ",'" + encodedPassword + "','" + addNameWorkerText.getText() + "', " + maxStage.getText() + ", '"+addStatus.getText()+"')";
        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(request);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }

        clear();
        refresh();
    }
    @FXML
    public void deleteFromProduct(ActionEvent event) {
        DataAccessorProducts db = DataAccessorProducts.getDataAccessor();

        String requestToDeleteFromProducts = "delete from products where art = " + deleteFromWorkersText.getText() + ";";

        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToDeleteFromProducts);
        } catch (SQLException e) {
            e.getMessage();
        }
        clear();
        refresh();
    }
    @FXML
    public void addDetail(ActionEvent event) {
        DataAccessorDetail db = DataAccessorDetail.getDataAccsessor();

        String request = "insert into details values (" + addIdWorkerText.getText() + ", '" + addPasswordWorkerText.getText() + "', '" + addNameWorkerText.getText() + "')";
        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(request);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }

        clear();
        refresh();
    }
}
