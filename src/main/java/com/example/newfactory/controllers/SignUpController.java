package com.example.newfactory.controllers;

import com.example.newfactory.dataAccessor.DataAccessorWorker;
import com.example.newfactory.entity.Security;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SignUpController {

    @FXML
    public Label error;
    public TextField nameText;
    public Button registrate;

    @FXML
    private TextField factoryId;

    @FXML
    private Button login;
    @FXML
    private Button close;

    @FXML
    private PasswordField password;


    public void userLogin(ActionEvent event) throws SQLException {

        if (checkLogin()) {
            onBtnLogin();
        }
    }

    public boolean checkLogin() {
        boolean result = false;

        if (!String.valueOf(factoryId).isBlank() && !password.getText().isBlank()) {
            String encodedPassword = Security.encrypt(password.getText());
            DataAccessorWorker db = DataAccessorWorker.getDataAccessor();
            String verifyLogin = "select count(1) from login where id = " + factoryId.getText() + " and password = '" + encodedPassword + "'";

            try {
                Statement statement = db.createStatement();
                ResultSet resultSet = statement.executeQuery(verifyLogin);

                while (resultSet.next()) {
                    if (resultSet.getInt(1) == 1) {
                        result = true;
                    } else {
                        result = false;
                        error.setText("Check ur id & password");
                        factoryId.clear();
                        password.clear();
                    }
                }
            } catch (SQLException e) {
                e.getMessage();
            }
        } else {
            error.setText("login and password required");
        }
        return result;
    }

    public void userRegistrate(ActionEvent event) {
        boolean result = false;

        if (!String.valueOf(factoryId).isBlank() && !password.getText().isBlank()) {

            String encodedPassword = Security.encrypt(password.getText());
            //String decodedPassword = Security.decrypt(encodedPassword);
            //String encodedPassword = String.valueOf(password.getText().hashCode());

            DataAccessorWorker db = DataAccessorWorker.getDataAccessor();
            String selection = "select count(1) from login where id = " + factoryId.getText() + " and password = '" + encodedPassword + "'";
            String registration = "insert into login values (" + factoryId.getText() + ",'" + encodedPassword + "','" + nameText.getText() + "')";
            //2023252005
            //2023252005
            try {
                Statement statement = db.createStatement();
                ResultSet resultSet1 = statement.executeQuery(selection);

                while (resultSet1.next()) {
                    if (resultSet1.getInt(1) == 0) {
                        result = true;
                    } else {
                        result = false;
                        error.setText("You already in here!!!");
                    }
                }
                if (result) {
                    ResultSet resultSet = statement.executeQuery(registration);
                    error.setText("You`ve been registered");
                }
            } catch (SQLException e) {
                e.getMessage();
            }
        } else {
            error.setText("login and password required");
        }
        factoryId.clear();
        password.clear();
        nameText.clear();
    }

    @FXML
    void onBtnLogin() {
        DataAccessorWorker dbLogin = DataAccessorWorker.getDataAccessor();
        String requestToGetStatusFromLogin = "select status from login where id = " + factoryId.getText() + "";
        String status = "";

        try {
            Statement statement = dbLogin.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToGetStatusFromLogin);
            while (resultSet.next()) {
                status = resultSet.getString(1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        switch (status) {
            case "admin" -> {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/AdminView.fxml"));

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                Parent root = fxmlLoader.getRoot();
                Stage stage = new Stage();
                stage.initStyle(StageStyle.UNDECORATED);
                stage.setScene(new Scene(root));
                stage.setTitle("Admin");
                stage.showAndWait();
            }
            case "god" -> {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/hello-view.fxml"));

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                Parent root = fxmlLoader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("КЕПКА!!!");
                stage.showAndWait();
            }
            case "engineer" -> {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/EngineerView.fxml"));

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                Parent root = fxmlLoader.getRoot();
                Stage stage = new Stage();
                stage.initStyle(StageStyle.UNDECORATED);
                stage.setScene(new Scene(root));
                stage.setTitle("Engineer");
                stage.showAndWait();
            }
            case "worker" -> {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/com/example/newfactory/WorkerView.fxml"));

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                Parent root = fxmlLoader.getRoot();
                Stage stage = new Stage();
                stage.initStyle(StageStyle.UNDECORATED);
                stage.setScene(new Scene(root));
                stage.setTitle("Worker");
                stage.showAndWait();
            }
        }
    }

    public void close(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }


}