package com.example.newfactory.controllers;

import com.example.newfactory.dataAccessor.DataAccessorAccept;
import com.example.newfactory.dataAccessor.DataAccessorDetail;
import com.example.newfactory.dataAccessor.DataAccessorProducts;
import com.example.newfactory.dataAccessor.DataAccessorWork;
import com.example.newfactory.entity.Detail;
import com.example.newfactory.entity.Product;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class WorkerController {

    public Label ERROR;
    public TableColumn detailArt;
    public TableColumn detailName;
    public TableColumn detailPrice;
    public TableColumn productArt;
    public TableColumn productName;
    public TableColumn productPrice;
    public TextField workerID;
    public Label detailInserting;
    public TableColumn maxStage;
    public Label yourStage;
    public Label maxStageNumber;
    public Button close;
    @FXML
    private TableView<Detail> detailTable;

    @FXML
    private Label error;

    @FXML
    private ImageView instructionField;

    @FXML
    private Button onBtnCreateProduct;

    @FXML
    private Button onBtnInsertDetail;

    @FXML
    private Button onBtnLearn;

    @FXML
    private TextField productIdText;

    @FXML
    private TableView<Product> productTable;
    @FXML
    private Image setImage;

    public void refresh(){
        createListDetails();
        createListProducts();
    }

    @FXML
    public void initialize(){
        refresh();
    }

    @FXML
    public void setOnBtnLearn(){
        createListDetails();
        setImage();
        createListProducts();
        error.setText("");
    }

    @FXML
    void createListDetails() {
        detailTable.getColumns().clear();
        detailTable.getItems().clear();
        DataAccessorDetail dataAccessorDetail = DataAccessorDetail.getDataAccsessor();

        TableColumn<Detail, String> name = detailName;
        TableColumn<Detail, String> art = detailArt;
        TableColumn<Detail, String> price = detailPrice;


        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        art.setCellValueFactory(new PropertyValueFactory<>("art"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));

        detailTable.getColumns().addAll(art, name, price);
        try {
            detailTable.getItems().addAll(dataAccessorDetail.getDetailsList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    void createListProducts() {
        productTable.getColumns().clear();
        productTable.getItems().clear();
        DataAccessorProducts dataAccessorProducts = DataAccessorProducts.getDataAccessor();

        TableColumn<Product, String> name = productName;
        TableColumn<Product, String> art = productArt;
        TableColumn<Product, String> price = productPrice;
        TableColumn<Product, String> maxstage = maxStage;


        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        art.setCellValueFactory(new PropertyValueFactory<>("art"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        maxstage.setCellValueFactory(new PropertyValueFactory<>("maxStage"));

        productTable.getColumns().addAll(art, name, price, maxstage);
        try {
            productTable.getItems().addAll(dataAccessorProducts.getProductsList());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    public void setOnBtnInsertDetail(ActionEvent event) {
        DataAccessorWork db = DataAccessorWork.getDataAccsessor();
        DataAccessorProducts dbProducts = DataAccessorProducts.getDataAccessor();

        String requestToIncrement = "update work set stage = stage + 1 where productid = " + productIdText.getText() + "  and factoryid ="+workerID.getText()+"";
        String requestToInsertIntoWork = "insert into work values (" + workerID.getText() + ", 0, 'In Progress', " + productIdText.getText() + ")";
        String requestToCheck = "select count(1) from work where factoryid = " + workerID.getText() + " and productid = " + productIdText.getText() + "";
        String requestToSoutStage = "select stage from work where factoryid = " + workerID.getText() + "";
        String requestToMaxStage = "select maxstage from products where art = " + productIdText.getText() + "";
        String requestToDone = "update work set status = 'Done!' where productid = " + productIdText.getText() + "  and factoryid ="+workerID.getText()+"";


        boolean check = false;


        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToCheck);
            while (resultSet.next()) {
                if (resultSet.getInt(1) == 0) {
                    check = true;
                } else {
                    check = false;
                }
            }
        } catch (SQLException e) {
            e.getMessage();
        }

        if (check) {
            try {
                Statement statement = dbProducts.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToMaxStage);

                while (resultSet.next()) {
                    maxStageNumber.setText(resultSet.getString(1));
                }
            } catch (SQLException e) {
                e.getMessage();
            }

            try {
                Statement statement = db.createStatement();
                ResultSet resultSet = statement.executeQuery(requestToInsertIntoWork);
                System.out.println(resultSet);
            } catch (SQLException e) {
                e.getMessage();
            }
        }

        try {
            Statement statement = dbProducts.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToMaxStage);

            if (Integer.parseInt(yourStage.getText())<Integer.parseInt(maxStageNumber.getText())){
                Statement statementToInc = db.createStatement();
                ResultSet resultSetToInc = statementToInc.executeQuery(requestToIncrement);
            } else if (Integer.parseInt(yourStage.getText())>=Integer.parseInt(maxStageNumber.getText())){
                Statement statementToMakeDone = db.createStatement();
                ResultSet resultSetToMakeDone = statementToMakeDone.executeQuery(requestToDone);
                error.setText("Enough!!!");
            }
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }

        try {
            Statement statement = db.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToSoutStage);

            while (resultSet.next()) {
                yourStage.setText(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @FXML
    public void setOnBtnCreateProduct(ActionEvent event) {
        DataAccessorAccept dbAccept = DataAccessorAccept.getDataAccsessor();
        DataAccessorWork dbWork = DataAccessorWork.getDataAccsessor();

        String requestInsertIntoAccept = "insert into accept values(" + workerID.getText() + "," + productIdText.getText() + ")";
        String requestToDeleteFromWork = "delete from work where factoryid = " + workerID.getText() + " and productid = " + productIdText.getText() + "";

        try {
            Statement statement = dbAccept.createStatement();
            ResultSet resultSet = statement.executeQuery(requestInsertIntoAccept);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }

        try {
            Statement statement = dbWork.createStatement();
            ResultSet resultSet = statement.executeQuery(requestToDeleteFromWork);
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.getMessage();
        }

        yourStage.setText("0");
        maxStageNumber.setText("0");
        error.setText("");

    }

    @FXML
    private void setImage() {
        DataAccessorProducts dbProducts = DataAccessorProducts.getDataAccessor();

        String requestCheck = "select count(1) from products where art = " + productIdText.getText() + "";
        boolean check = false;

        try {
            Statement statement = dbProducts.createStatement();
            ResultSet resultSet = statement.executeQuery(requestCheck);

            while (resultSet.next()) {
                if (resultSet.getInt(1) >= 1) {
                    check = true;
                } else {
                    check = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        int productId = Integer.parseInt(productIdText.getText());
        String imageUrl;

        //TODO надо короче сделать проверку по каунту в базу изделий и выводить ошибку если 0
        if (check) {
            imageUrl = "/Instructions/" + productId + ".png";
        } else {
            imageUrl = "/Instructions/0.png";
        }

        URL imageURLUrl = getClass().getResource(imageUrl);
        if (imageURLUrl != null) {
            Image image = new Image(imageURLUrl.toString());
            instructionField.setImage(image);
        } else {
            System.err.println("Image not found: " + imageUrl);
        }
    }
    public void closed(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }
}
