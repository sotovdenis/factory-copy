module com.example.newfactory {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires org.postgresql.jdbc;
    requires junit;


    opens com.example.newfactory to javafx.fxml;
    exports com.example.newfactory;
    exports com.example.newfactory.controllers;
    opens com.example.newfactory.controllers to javafx.fxml;
    exports com.example.newfactory.dataAccessor;
    opens com.example.newfactory.dataAccessor to javafx.fxml;
    exports com.example.newfactory.entity;
    opens com.example.newfactory.entity to javafx.fxml;
    exports com.example.newfactory.entity.users;
    opens com.example.newfactory.entity.users to javafx.fxml;
}