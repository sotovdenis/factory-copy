package tests;

import com.example.newfactory.entity.Security;

import java.util.Scanner;

public class SecurityTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String password = scanner.nextLine();

        System.out.println(Security.encrypt(password));
        String encoded = Security.encrypt(password);
        System.out.println();
        System.out.println(Security.decrypt(encoded));
    }
}
