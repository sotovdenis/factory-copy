package tests;

import com.example.newfactory.dataAccessor.DataAccessorWorker;
import com.example.newfactory.entity.users.Worker;
import org.junit.*;
import java.sql.SQLException;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class WorkerListTest {
    @Test
    public void idTest() throws SQLException {
        DataAccessorWorker dataAccessorWorker = DataAccessorWorker.getDataAccessor();
        List<Worker> workers = dataAccessorWorker.getWorkersList();
        assertEquals(workers.toString(), Worker.getTest(workers).toString());
    }
}
